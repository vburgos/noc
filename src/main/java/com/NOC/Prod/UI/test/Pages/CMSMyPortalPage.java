package com.NOC.Prod.UI.test.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.NOC.Prod.UI.test.Utilities.BaseClass;


public class CMSMyPortalPage  extends BaseClass{
	
	public CMSMyPortalPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="cms-noapps-access")
	public WebElement UATNoAccessApp;
	
	@FindBy(id="stPageFrame_AdvancedProviderScreeningStaging")
	public WebElement APSStaging;
	
	@FindBy(id="cms_AdvancedProviderScreening-Staging_SIT_pidb")
	public WebElement APSSITLink;
	
	@FindBy(id="cms_AdvancedProviderScreening_tileid")
	public WebElement APSUAT;
	
	@FindBy(id="cms_AdvancedProviderScreening_Application_pidb")
	public WebElement APSUATLink;
	
	@FindBy(xpath="//span[contains(text(), 'VPT')]")
	public WebElement APSVPTLink;
	
	@FindBy(id="cms_PECOSAI_tileid")
	public WebElement PECOSTile;
	

}
