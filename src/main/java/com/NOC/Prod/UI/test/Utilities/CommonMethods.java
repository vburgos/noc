package com.NOC.Prod.UI.test.Utilities;

import java.io.File;
import java.io.IOException;
import org.codehaus.plexus.util.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



public class CommonMethods extends BaseClass {
	
	public static String verifyTitle() {
		return driver.getTitle();
	}
	
	public static void getText(WebElement element) {
		System.out.println(element.getText());
	}
	
	public static void getAttribute(WebElement element) {
		System.out.println(element.getAttribute("value"));
	}
	
	public static void clickLink(String linkText) {
		driver.findElement(By.linkText(linkText)).click();
	}
	
	public static void clickPartialLink(String linkText) {
		driver.findElement(By.partialLinkText(linkText)).click();
	}
	
	public static void enterValue(WebElement element, String value) {
		element.clear();
		element.sendKeys(value);
	}

	public static WebElement waiting(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		return wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	public static WebElement waiting50Secs(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, 57);
		return wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	
	public static WebElement waitUntilVisibile(String element) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(element)));
	}
	
	public static WebElement waitUntilVisibileID(String element) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(element)));
	}

	public static void click(WebElement element) {
		WebElement elm = waiting(element);
		elm.click();
	}
	
	public static void Longwaitclick(WebElement element) {
		
		WebElement elm = (new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOf(element));
		elm.click();
	}
	
	public static void selectValueVisibleText(WebElement element, String value) {
		Select obj = new Select(element);
		obj.selectByVisibleText(value);
	}
	
	public static void selectByValue(WebElement element, String value) {
		Select obj = new Select(element);
		obj.selectByValue(value);
	}
	
	public static void handlingOKBttnAlerts() {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			System.out.println(alert.getText());
			alert.accept();
			} catch (Exception e) {
				//exception handling
		}
	
	}
	
	public static void switchingFrames(String frameXpath) {
		driver.switchTo().frame(driver.findElement(By.xpath(frameXpath)));
	}
	
	public static void switchingBackToDefault() {
		driver.switchTo().defaultContent();
	}
	
	
	public static void captureScreenshot (WebDriver driver, String screenshotName) {
		TakesScreenshot ts = (TakesScreenshot)driver;
		File file = ts.getScreenshotAs(OutputType.FILE);
		
		try {
			FileUtils.copyFile(file, new File("./ScreenShots/"+screenshotName+".png"));
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
}
