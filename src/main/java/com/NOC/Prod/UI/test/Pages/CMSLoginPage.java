package com.NOC.Prod.UI.test.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.NOC.Prod.UI.test.Utilities.BaseClass;


public class CMSLoginPage extends BaseClass{
	
	public CMSLoginPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(css="#cms-homepage-login-logo")
	public WebElement CMSlogo;
	
	@FindBy(css="#cms-login-userid")
	public WebElement username;
	
	@FindBy(css="#cms-login-password")
	public WebElement password;
	
	@FindBy(css="#checkd")
	public WebElement CTAgreeBox;
	
	@FindBy(id="cms-login-submit")
	public WebElement LoginBTN;

}
