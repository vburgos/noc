package com.NOC.Prod.UI.test.Utilities;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BaseClass {

	public static Properties prop;
	public static WebDriver driver;

	public static void setUp() {

		initProperties(Constants.filePath);
		String browserName = prop.getProperty("browser");
		String userName = prop.getProperty("username");
		String password = prop.getProperty("password");

		if (browserName.equalsIgnoreCase("chrome")) {
			if (Constants.osName.contains("Mac")) {
				System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver");
			} else if (Constants.osName.contains("Windows")) {
				System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver.exe");
			}

			driver = new ChromeDriver();

		} else if (browserName.equalsIgnoreCase("ie")) {
			if (Constants.osName.contains("Mac")) {
				System.setProperty("webdriver.ie.driver", "Drivers/IEDriverServer");
			} else if (Constants.osName.contains("Windows")) {
				System.setProperty("webdriver.ie.driver", "Drivers/IEDriverServer.exe");
			}
			driver = new InternetExplorerDriver();
			driver.manage().deleteAllCookies();
		}
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		if (Constants.osName.contains("Mac")) {
			driver.manage().window().fullscreen();
		} else if (Constants.osName.contains("Windows")) {
			driver.manage().window().maximize();
		}
		driver.get(prop.getProperty("url"));
			
		driver.findElement(By.id("cms-login-userid")).sendKeys(userName);
		driver.findElement(By.id("cms-login-password")).sendKeys(password);
		if(browserName.equalsIgnoreCase("chrome")) {
		driver.findElement(By.id("cms-label-tc")).click();
		} 
		driver.findElement(By.id("cms-login-submit")).click();
	}

	public static void tearDown() {
		driver.close();
		driver.quit();
	}

	public static void initProperties(String filePath) {

		prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(filePath);
			prop.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
