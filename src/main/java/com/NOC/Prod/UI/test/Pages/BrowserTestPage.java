package com.NOC.Prod.UI.test.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.NOC.Prod.UI.test.Utilities.BaseClass;


public class BrowserTestPage extends BaseClass {
	public BrowserTestPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//*[@id='divLogo']/img")
	public WebElement logo;
	
	@FindBy(id="txtUsername")
	public WebElement username;
	
	@FindBy(id="txtPassword")
	public WebElement password;
	
	@FindBy(id="btnLogin")
	public WebElement btnLogin;
	
	@FindBy(id="spanMessage")
	public WebElement errorMessage;
	
	

}
