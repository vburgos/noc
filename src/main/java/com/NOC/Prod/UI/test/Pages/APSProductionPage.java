package com.NOC.Prod.UI.test.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.NOC.Prod.UI.test.Utilities.BaseClass;


public class APSProductionPage extends BaseClass {
	
	public APSProductionPage() {
		PageFactory.initElements(driver, this);
	}
	
	//Portal Page Elements
	
	@FindBy(id="APSProduction123ReleasePage")
	public WebElement ProductionAPSTile;
	
	@FindBy(xpath="//span[contains(text(), 'Application')]")
	public WebElement ProductionAPSLink;
	
	@FindBy(id="cms_PECOSAI_tileid")
	public WebElement ProductionPECOSTile;
	
	@FindBy(id="cms_PECOSAI_Application_pidb")
	public WebElement ProductionPECOSLink;
	
	//PECOS Elements
	
	@FindBy(className="tabDeselected")
	public WebElement PECOSSearchTab;
	
	@FindBy(xpath="//a[contains(text(),'Search')]")
	public WebElement PECOSSearch;
	
	//PECOS Search Elements
	
	@FindBy(id="id_enrollmentID")
	public WebElement PECOSEnrollmentIDBox;
	
	@FindBy(id="basicResultType1")
	public WebElement PECOSFinalizedEnrollmentBtn;
	
	@FindBy(id="basicSEARCH")
	public WebElement PECOSSearchBtn;
	
	//PECOS Table
	
	@FindBy(id="enrollmentSearchResults")
	public WebElement PECOSSearchTable;
	
	@FindBy (css= "table[id=enrollmentSearchResults] tr td:nth-child(11)")
	public WebElement ColApproved;
	
	
	
	
	
	
	
	
	

}
