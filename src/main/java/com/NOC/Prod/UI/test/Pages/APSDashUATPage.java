package com.NOC.Prod.UI.test.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.NOC.Prod.UI.test.Utilities.BaseClass;


public class APSDashUATPage extends BaseClass{
	
	public APSDashUATPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="cms-noapps-access")
	public WebElement UATNoAccessApp;
	
	@FindBy(xpath="//button/b[contains(text(),'OK')]")
	public WebElement UATAcceptBtN;
	
	@FindBy(xpath="//button/b[contains(text(),'OK')]")
	public WebElement VPTAcceptBtN;
	
	@FindBy(xpath="//*[@id='reportSelection']/a[contains(text(),'REPORTS')]")
	public WebElement ReportsUAT;
	
	@FindBy(xpath="//*[@id='reportSelection']")
	public WebElement ReportsDDMenuUAT;
	
	@FindBy(xpath="//a[contains(text(),'LCM REPORT')]")
	public WebElement LCMReportUAT;
	
	//Search Tab
	@FindBy(xpath="//a[contains(text(),'SEARCH')]")
	public WebElement SearchTabUAT;
	
	@FindBy(id="entityRadio")
	public WebElement SearchEntityUAT;
	
	@FindBy(id="enrollRadio")
	public WebElement SearchEnrollUAT;
	
	@FindBy(id="individualImage")
	public WebElement SearchIndividualUAT;
	
	@FindBy(id="firstName")
	public WebElement SearchFNTextUAT;
	
	@FindBy(id="searchBtn")
	public WebElement SearchBtnUAT;
	
	@FindBy(id="searchResultTable")
	public WebElement SearchResultsTableUAT;
	
	@FindBy(id="backToSearchBtn")
	public WebElement SearchBackBtnUAT;
	
	//Search Result
	@FindBy(xpath="//a[@href='viewProfile-J3Ub8TZfW0K9gLw8rddzVlSIqh1cma4u.htm']")
	public WebElement SearchResultProfileUAT;
	
	@FindBy(xpath="//a[@href='viewProfile-bcmtxFhrzu-2-s_1wUrhwyOMiKnxxGWJ.htm']")
	public WebElement SearchResultProfileSIT;
	
	//Search Profile Page
	@FindBy(id="crimLabel")
	public WebElement profileCheckCriminalUAT;
	
	@FindBy(id="fedLabel")
	public WebElement profileCheckFederalUAT;
	
	//Profile Tab
	@FindBy(xpath="//a[contains(text(), 'PROFILE')]")
	public WebElement profileTabUAT;
	
	@FindBy(xpath="//a[contains(text(), 'CREATE PROFILE')]")
	public WebElement createProfileLinkUAT;
	
	//Profile Page
	@FindBy(id="ind")
	public WebElement profileIndivUAT;
	
	@FindBy(xpath="//span[contains(text(), 'Supporting Documentation')]")
	public WebElement SuppportDocTitle;
	
	@FindBy(id="org")
	public WebElement profileOrgUAT;
	
	//Reports Tab
	@FindBy(xpath="//a[contains(text(), 'CCM REPORT')]")
	public WebElement CCMReportLinkUAT;
	
	@FindBy(xpath="//a[contains(text(), 'DNP REPORT')]")
	public WebElement DNPReportLinkUAT;
	
	@FindBy(xpath="//a[contains(text(), 'REPORTING CONSOLE')]")
	public WebElement ReportConsoleLinkUAT;
	
	@FindBy(xpath="//a[contains(text(), 'REAL-TIME REPORTS')]")
	public WebElement ReportTimeLinkUAT;
	
	@FindBy(xpath="//a[contains(text(), 'REJECTED RECORDS')]")
	public WebElement RejectedRecLinkUAT;
	
	@FindBy(xpath="//a[contains(text(), 'REPORTS')]")
	public WebElement ReportsLinkUAT;

}
