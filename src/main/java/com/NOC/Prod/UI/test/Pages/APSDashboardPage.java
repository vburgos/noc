package com.NOC.Prod.UI.test.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.NOC.Prod.UI.test.Utilities.BaseClass;

public class APSDashboardPage extends BaseClass {
	
	public APSDashboardPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//button/b[contains(text(),'OK')]")
	public WebElement AcceptBtN;
	
	@FindBy(xpath="//*[@id='reportSelection']/a[contains(text(),'REPORTS')]")
	public WebElement Reports;
	
	@FindBy(xpath="//*[@id='reportSelection']")
	public WebElement ReportsDDMenu;
	
	@FindBy(xpath="//a[contains(text(),'LCM REPORT')]")
	public WebElement LCMReport;
	

}
