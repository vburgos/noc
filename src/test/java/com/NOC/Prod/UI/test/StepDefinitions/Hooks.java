package com.NOC.Prod.UI.test.StepDefinitions;


import com.NOC.Prod.UI.test.Utilities.BaseClass;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	
	@Before
	public void start() {
		BaseClass.setUp();
	}
	
	@After
	public void end() {
		BaseClass.tearDown();
	}

}
