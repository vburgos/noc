package com.NOC.Prod.UI.test.StepDefinitions;

import org.openqa.selenium.By;

import com.NOC.Prod.UI.test.Pages.APSDashUATPage;
import com.NOC.Prod.UI.test.Utilities.CommonMethods;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class APSUATUserTestStepDefinitions {
	
	APSDashUATPage APSUAT;
	
	@When("Terms and Conditions window appears on UAT and press ok button")
	public void terms_and_Conditions_window_appears_on_UAT_and_press_ok_button() throws InterruptedException {
		
		APSUAT = new APSDashUATPage();
		Thread.sleep(2000);
		CommonMethods.waitUntilVisibileID("ns_Z7_9G4004G0P05Q50Q6V5DE7930I7__content-frame");
		CommonMethods.switchingFrames("//iframe[@id='ns_Z7_9G4004G0P05Q50Q6V5DE7930I7__content-frame']");
		CommonMethods.waiting(APSUAT.UATAcceptBtN);
		CommonMethods.click(APSUAT.UATAcceptBtN);
		CommonMethods.switchingBackToDefault();
	}
	
	@When("In VPT, Terms and Conditions window appears and press ok button")
	public void in_VPT_Terms_and_Conditions_window_appears_and_press_ok_button() throws InterruptedException {
		APSUAT = new APSDashUATPage();
		Thread.sleep(2000);
		CommonMethods.waitUntilVisibileID("ns_Z7_9G4004G0P07M80QM2PP97620C0__content-frame");
		CommonMethods.switchingFrames("//iframe[@id='ns_Z7_9G4004G0P07M80QM2PP97620C0__content-frame']");
		CommonMethods.waiting(APSUAT.VPTAcceptBtN);
		CommonMethods.click(APSUAT.VPTAcceptBtN);
		CommonMethods.switchingBackToDefault();
	}

}
