package com.NOC.Prod.UI.test.Runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;

import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features= {"src/test/resources/features"}
		, plugin = { "html:target/cucumber-html-report",
				"json:target/cucumber-report/cucumber.json", "pretty:target/cucumber-report/cucumber-pretty.txt",
				"usage:target/cucumber-report/cucumber-usage.json",
				"junit:target/cucumber-report/cucumber-results.xml" }
		, glue= {"com.NOC.Prod.UI.test.StepDefinitions"}
		, dryRun = true
		, monochrome = true
		)

public class TestRunner {

}
