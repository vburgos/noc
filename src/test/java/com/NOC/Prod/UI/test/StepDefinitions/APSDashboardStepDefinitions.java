package com.NOC.Prod.UI.test.StepDefinitions;



import com.NOC.Prod.UI.test.Pages.APSDashboardPage;
import com.NOC.Prod.UI.test.Utilities.CommonMethods;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class APSDashboardStepDefinitions {
	
	APSDashboardPage DB;
	
	@When("Terms and Conditions window appears and press ok button")
	public void terms_and_Conditions_window_appears_and_press_ok_button() {
	    
		DB = new APSDashboardPage();
		CommonMethods.waitUntilVisibile("ns_Z7_9G4004G0P07M80QM2PP97620K5__content-frame");
		CommonMethods.switchingFrames("//iframe[@id='ns_Z7_9G4004G0P07M80QM2PP97620K5__content-frame']");
		CommonMethods.waiting(DB.AcceptBtN);
		CommonMethods.click(DB.AcceptBtN);
		CommonMethods.switchingBackToDefault();
	}

	@When("I enter the APS portal page")
	public void i_enter_the_APS_portal_page() {
	    
		DB.driver.getTitle().equalsIgnoreCase("CMS Enterprise Portal - SIT");
	}

	@Then("I click on the Reports drop down menu and choose LCM Report")
	public void i_click_on_the_Reports_drop_down_menu_and_choose_LCM_Report() {
	   
		CommonMethods.waitUntilVisibile("ns_Z7_9G4004G0P07M80QM2PP97620K5__content-frame");
		CommonMethods.switchingFrames("//iframe[@id='ns_Z7_9G4004G0P07M80QM2PP97620K5__content-frame']");
		CommonMethods.click(DB.Reports);
		CommonMethods.click(DB.LCMReport);
		
	}

	@Then("In UAT, I click on the Reports drop down menu and choose LCM Report")
	public void in_UAT_I_click_on_the_Reports_drop_down_menu_and_choose_LCM_Report() {
		
		DB = new APSDashboardPage();
		CommonMethods.waitUntilVisibile("ns_Z7_79G0I341NG4B40AF7AGRVV3004__content-frame");
		CommonMethods.switchingFrames("//iframe[@id='ns_Z7_79G0I341NG4B40AF7AGRVV3004__content-frame']");
		CommonMethods.click(DB.Reports);
		CommonMethods.click(DB.LCMReport);
	}


}
