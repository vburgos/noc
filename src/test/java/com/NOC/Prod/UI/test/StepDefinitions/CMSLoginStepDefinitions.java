package com.NOC.Prod.UI.test.StepDefinitions;

import org.openqa.selenium.By;

import com.NOC.Prod.UI.test.Pages.CMSMyPortalPage;
import com.NOC.Prod.UI.test.Utilities.CommonMethods;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CMSLoginStepDefinitions {
	
	CMSMyPortalPage MP;

	@When("I click on APS Staging followed by clicking on UAT link")
	public void i_click_on_APS_Staging_followed_by_clicking_on_UAT_link() throws InterruptedException {
		MP = new CMSMyPortalPage();
		CommonMethods.click(MP.APSUAT);
		CommonMethods.click(MP.APSUATLink);
		Thread.sleep(5000);
		CommonMethods.captureScreenshot(MP.driver, "MyPortalPageResultUAT");
	}
	
	@Then("I click on APS Staging and no apps are available and close site")
	public void i_click_on_APS_Staging_and_no_apps_are_available_and_close_site() throws InterruptedException {
		Thread.sleep(3000);
		boolean isPresent = MP.driver.findElement(By.id("cms-noapps-access")).getText().contains("Use the below link to request access to CMS Systems/Applications.");
		
		if (isPresent) {
			System.out.println("User has no Job Codes to access APS and is not allowed to go any further");
			CommonMethods.captureScreenshot(MP.driver, "UserWithNoJobCodesAccess");
			System.out.println("End of Test");;
		}
	}

}
