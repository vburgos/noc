$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/NOCProductionUI.feature");
formatter.feature({
  "name": "Verifying Production Site Login",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Login to Production site",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "I enter My Portal dashboard and see My Portal",
  "keyword": "When "
});
formatter.match({
  "location": "CMSMyPortalStepDefinitions.i_enter_My_Portal_dashboard_and_see_My_Portal()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click on APS Staging followed by clicking on UAT link",
  "keyword": "And "
});
formatter.match({
  "location": "CMSLoginStepDefinitions.i_click_on_APS_Staging_followed_by_clicking_on_UAT_link()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Terms and Conditions window appears on UAT and press ok button",
  "keyword": "And "
});
formatter.match({
  "location": "APSUATUserTestStepDefinitions.terms_and_Conditions_window_appears_on_UAT_and_press_ok_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I enter the APS portal page",
  "keyword": "And "
});
formatter.match({
  "location": "APSDashboardStepDefinitions.i_enter_the_APS_portal_page()"
});
formatter.result({
  "status": "skipped"
});
});