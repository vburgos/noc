Feature: Verifying Production Site Login

  Scenario: Login to Production site                                   # src/test/resources/features/NOCProductionUI.feature:4
    When I enter My Portal dashboard and see My Portal                 # CMSMyPortalStepDefinitions.i_enter_My_Portal_dashboard_and_see_My_Portal()
    And I click on APS Staging followed by clicking on UAT link        # CMSLoginStepDefinitions.i_click_on_APS_Staging_followed_by_clicking_on_UAT_link()
    And Terms and Conditions window appears on UAT and press ok button # APSUATUserTestStepDefinitions.terms_and_Conditions_window_appears_on_UAT_and_press_ok_button()
    And I enter the APS portal page                                    # APSDashboardStepDefinitions.i_enter_the_APS_portal_page()
